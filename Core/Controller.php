<?php

class Controller
{
	public function __construct() {
		$this->model(get_class($this));
	}

	public function model($model)
	{
		$model_calss_name = $model . 'Model';

		if (file_exists( APP_DIR . DIRECTORY_SEPARATOR . 'Models' . DIRECTORY_SEPARATOR . ucfirst($model) . '.php' )) {
			require_once APP_DIR . DIRECTORY_SEPARATOR . 'Models' . DIRECTORY_SEPARATOR . ucfirst($model) . '.php';
			return new $model_calss_name();
		}
	}

	public function view($view,$data = [])
	{
		if (file_exists( APP_DIR . DIRECTORY_SEPARATOR . 'Views' . DIRECTORY_SEPARATOR . strtolower($view) . '.php' )) {
			require_once APP_DIR . DIRECTORY_SEPARATOR . 'Views' . DIRECTORY_SEPARATOR . strtolower($view) . '.php';
		}
	}
}