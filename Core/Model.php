<?php

class Model
{
	public function getJSON($type)
	{
		$data = json_decode( file_get_contents(DATA_DIR . DIRECTORY_SEPARATOR . strtolower($type) . '.json') );

		return $data;
	}
}