<?php

function base_url($subdir){
	return sprintf(
		"%s://%s%s",
		isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
		$_SERVER['SERVER_NAME'],
		DIRECTORY_SEPARATOR . $subdir .DIRECTORY_SEPARATOR
	);
}

define('BASE_URL',base_url('da-mvc-testine'));
define('ROOT_DIR', dirname(__DIR__));
define('APP_DIR', ROOT_DIR .DIRECTORY_SEPARATOR. 'App');
define('CORE_DIR', ROOT_DIR .DIRECTORY_SEPARATOR. 'Core');
define('DATA_DIR', ROOT_DIR .DIRECTORY_SEPARATOR. 'Data');
define('ASSETS_URL', BASE_URL . '/assets/');

require_once 'App.php';
require_once 'Controller.php';
require_once 'Model.php';