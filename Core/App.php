<?php

class App
{
	protected $controller = 'home';
	protected $action = 'index';
	protected $params = [];

	public function __construct() {

		$segments = $this->parse_url_segments();

		if (file_exists( APP_DIR . DIRECTORY_SEPARATOR . 'Controllers' . DIRECTORY_SEPARATOR . ucfirst($segments[0]) . '.php' )) {
			$this->controller = ucfirst($segments[0]);
			unset($segments[0]);
		}

		require_once APP_DIR . DIRECTORY_SEPARATOR . 'Controllers' . DIRECTORY_SEPARATOR . ucfirst($this->controller) . '.php';

		$this->controller = new $this->controller;


		if (isset($segments[1])) {
			if (is_callable([$this->controller,$segments[1]])) {
				$this->action = $segments[1];
				unset($segments[1]);
			}
		}

		$this->params = (is_array($segments) ? array_values($segments) : []);

		call_user_func_array( [$this->controller, $this->action], $this->params );
	}

	public function parse_url_segments() {

		$url = ($_GET['segments'] ?? false);

		if (!$url) {
			return false;
		}

		$url = rtrim($url,'/');
		$url_expl = explode('/',$url);

		return $url_expl;

	}

}