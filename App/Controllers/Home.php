<?php

class Home extends Controller
{
	public function __construct() {
		parent::__construct();
	}

	public function index() {

		$products = $this->model( 'products' );
		$cart = $this->model('cart');

		$data['products'] = $products->getAll();
		$data['cart_total'] = count($cart->getCart());

		$this->view(get_class($this),$data);

	}
}