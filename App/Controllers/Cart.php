<?php

class Cart extends Controller
{
	public function __construct() {
		parent::__construct();
	}

	public function index()
	{
		$cart = $this->model( 'cart' );
		$products = $this->model( 'products' );


		$product_ids = $cart->getCart();

		$data['products'] = $products->getById($product_ids);
		$data['cart_total'] = count($cart->getCart());

		if (empty($data['products'])) {
			header('Location: ' . BASE_URL);
		}
		
		$this->view(get_class($this),$data);
	}

	public function countItems()
	{
		$cart = $this->model( 'cart' );
	}

	public function remove($id)
	{
		$cart = $this->model( 'cart' );

		if ($cart->removeItem($id)) {
			header('Location: ' . BASE_URL . '/cart');
		}

		header('Location: ' . BASE_URL);
	}

	public function add($id)
	{
		$cart = $this->model( 'cart' );

		if ($cart->addItem($id)) {
			header('Location: ' . BASE_URL . '/cart');
		}

		header('Location: ' . BASE_URL);
	}
}