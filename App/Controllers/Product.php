<?php

class Product extends Controller
{
	public function __construct() {
		parent::__construct();
	}

	public function index($params)
	{
		$products = $this->model( 'products' );
		$cart = $this->model('cart');

		$data['product'] = $products->getById($params);
		$data['cart_total'] = count($cart->getCart());

		$this->view(get_class($this),$data);
	}
}