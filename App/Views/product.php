<?php include 'partials/header.php'; ?>

<div class="container">
	<div class="product">
		<div class="image">
			<img src="https://via.placeholder.com/350x200" alt="">
			<div class="price"><?php echo $data['product']->price; ?> &euro;</div>
		</div>
		<div class="name"><h2><?php echo $data['product']->name; ?></h2></div>
		<div class="desc"><p><?php echo $data['product']->desc; ?></p></div>
		<div class="actions">
			<a href="<?php echo BASE_URL ?>">Go back</a>
			<a href="<?php echo BASE_URL ?>cart/add/<?php echo $data['product']->id; ?>">Buy</a>
		</div>
	</div>
</div>

<?php include 'partials/footer.php'; ?>
