<!doctype html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title></title>
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500&amp;subset=latin-ext" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo ASSETS_URL ?>css/normalize.css">
	<link rel="stylesheet" type="text/css" href="<?php echo ASSETS_URL ?>css/main.css">
</head>
<body>

<div class="container">
	<div class="header">
		<div><a href="<?php echo BASE_URL ?>">BRAND</a></div>
		<div><a href="<?php echo BASE_URL ?>cart">CART (<?php echo $data['cart_total'] ?>)</a></div>
	</div>
</div>