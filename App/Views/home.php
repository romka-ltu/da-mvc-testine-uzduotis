<?php include 'partials/header.php'; ?>

<div class="container">
	<div class="products">
		<?php foreach($data['products'] as $product) : ?>
			<div class="product">
				<div class="image">
					<img src="https://via.placeholder.com/350x200" alt="">
					<div class="price"><?php echo $product->price; ?> &euro;</div>
				</div>
				<div class="name"><h2><?php echo $product->name; ?></h2></div>
				<div class="desc"><p><?php echo $product->desc; ?></p></div>
				<div class="actions">
					<a href="<?php echo BASE_URL ?>product/<?php echo $product->id; ?>">Read more</a>
					<a href="<?php echo BASE_URL ?>cart/add/<?php echo $product->id; ?>">Buy</a>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</div>

<?php include 'partials/footer.php'; ?>