<?php

class CartModel extends Model
{
	public function getCart()
	{
		return $this->getJSON('cart');
	}

	private function write($cart_items)
	{
		$file = fopen(DATA_DIR . DIRECTORY_SEPARATOR . 'cart.json', "w");

		flock($file, LOCK_EX);
		ftruncate($file, 0);
		$writed = fwrite($file, json_encode($cart_items));
		fflush($file);
		flock($file, LOCK_UN);
		fclose($file);

		return $writed;
	}

	public function removeItem($id)
	{
		$cart_items = $this->getJSON('cart');

		$cart_items = array_diff($cart_items, [$id]);

		return $this->write(array_values($cart_items));
	}

	public function addItem($id)
	{
		$cart_items = $this->getJSON('cart');

		if((array_search($id, $cart_items)) === false) {
			array_push($cart_items,$id);
			$this->write($cart_items);
			return true;
		}

		return false;
	}
}