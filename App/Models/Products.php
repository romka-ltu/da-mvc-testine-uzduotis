<?php

class ProductsModel extends Model
{
	public function getAll()
	{
		return $this->getJSON( 'Products' );
	}

	public function getById($id)
	{
		$products = $this->getJSON( 'Products' );
		$cart_items = [];

		foreach ($products as $k => $product)
		{
			if (is_array($id)) {

				if (in_array($product->id,$id)) {
					$cart_items[] = $products[$k];
				}

			} else {

				if ($product->id == $id) {
					return $products[$k];
				}

			}
		}

		return $cart_items;
	}


}